var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpackConfig = require('./webpack.config.js');

gulp.task('prod', ['webpack:prod', 'copy']);
gulp.task('dev', ['webpack:dev', 'copy']);
gulp.task('dev-server', ['webpack-dev-server']);

gulp.task('webpack:prod', function(callback) {

  var config = Object.create(webpackConfig);
  config.watch = false;
  config.devtool = '';
  config.plugins = config.plugins.concat([
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new HtmlWebpackPlugin({
      template: path.resolve('src', 'popup.html'),
      inject: 'body',
      filename: 'popup.html'
    }),
    new webpack.DefinePlugin({
      __DEVELOPMENT__: false,
      __DEVTOOLS__: false,
      __HOTRELOAD__: false
    })
  ]);

  webpack(config, function(err, stats) {
    if(err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true
    }));
    callback();
  });
});

gulp.task('webpack:dev', function(callback) {

  var config = Object.create(webpackConfig);
  config.devtool = 'source-map';
  config.plugins = config.plugins.concat([
    new HtmlWebpackPlugin({
      template: path.resolve('src', 'popup.html'),
      inject: 'body',
      filename: 'popup.html'
    }),
    new webpack.DefinePlugin({
      __DEVELOPMENT__: true,
      __DEVTOOLS__: false,
      __HOTRELOAD__: false
    })
  ]);

  webpack(config, function(err, stats) {
    if(err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true
    }));
    callback();
  });
});

gulp.task('webpack-dev-server', function(callback) {
    var config = Object.create(webpackConfig);
    config.devtool = 'eval-source-map';
    config.plugins = config.plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        template: path.resolve('src', 'popup.html'),
        inject: 'body'
      }),
      new webpack.DefinePlugin({
        __DEVELOPMENT__: true,
        __DEVTOOLS__: true,
        __HOTRELOAD__: true
      })
    ]);

    new WebpackDevServer(webpack(config), {
      publicPath: config.output.publicPath,
      hot: true,
      historyApiFallback: true,
      proxy: {
        '/api/*': 'http://localhost:3001' // TODO: change URL to the API
      },
      stats: {
        colors: true
      }
    }).listen(3000, 'localhost', function (err) {
      if(err) throw new gutil.PluginError('webpack-dev-server', err);
      gutil.log('[webpack-dev-server]', 'http://localhost:3000/webpack-dev-server/index.html');
    });
});

gulp.task('copy', function() {
  gulp.src([
    './src/manifest.json',
    './src/popup.html'
  ]).pipe(gulp.dest('./dist'));
});
