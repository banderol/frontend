import * as types from '../constants/ActionTypes';

export function fetchItems() {
  return dispatch => {
    dispatch({
      type: types.FETCH_ITEMS,
      items: [{ id: 1, description: 'test1', ttn: 'test1' }]
    });
  };
}

export function trackItem(item) {
  return dispatch => {
    dispatch({
      type: types.ADD_ITEM,
      item: item
    });
  };
}
