import React, { PropTypes, Component } from 'react';
import './creation.css';

const emptyItem = { ttn: '' };

class ItemCreation extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = Object.assign({}, emptyItem);
  }

  render() {
    return (
      <div className="creation">
        <span className="label">Please enter your Tracking Number</span>
        <input ref="ttnInput" type="text" placeholder="Tracking Number" value={this.state.ttn} onChange={this.handleChange.bind(this)} />
        <button onClick={this.handleClick.bind(this)}>Track</button>
      </div>
    );
  }

  handleChange() {
    this.setState({
      ttn: this.refs.ttnInput.getDOMNode().value
    });
  }

  handleClick() {
    if (this.state.ttn !== '') {
      this.props.trackItem(this.state);
      this.setState(Object.assign({}, emptyItem));
    }
  }
}

ItemCreation.propTypes = {
  trackItem: PropTypes.func.isRequired
};

export default ItemCreation;
