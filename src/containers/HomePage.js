import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ItemCreation } from '../components';
import * as ItemActions from '../actions/ItemActions';

class Home extends Component {

  render() {
    const { items, dispatch } = this.props;
    const actions = bindActionCreators(ItemActions, dispatch);

    return (
      <div className="home-page">
        <ItemCreation trackItem={actions.trackItem} />
          <ul className="items-list">
            {items.map(item =>
              <li>{item.ttn}</li>
            )}
          </ul>
      </div>
    );
  }
}

Home.propTypes = {
  items: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
};

function select(state) {
  return {
    items: state.items
  };
}

export default connect(select)(Home);
