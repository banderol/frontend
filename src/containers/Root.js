import React, { Component, PropTypes} from 'react';
import { Redirect, Router, Route } from 'react-router';
import { Provider } from 'react-redux';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
import configureStore from '../store/configureStore';
import App from './App';
import HomePage from './HomePage';

const store = configureStore();

export default class Root extends Component {
  render() {
    let devTools = '';
    if (__DEVTOOLS__) {
      devTools = (
        <DebugPanel top right bottom>
          <DevTools store={store}
             monitor={LogMonitor}
             visibleOnLoad={true} />
        </DebugPanel>
      );
    }

    return (
      <div>
        <Provider store={store}>
          {() =>
            <Router history={this.props.history}>
              <Route component={App}>
                <Route path="home" component={HomePage} />
                <Redirect from="*" to="/home" />
              </Route>
            </Router>
          }
        </Provider>
        {devTools}
      </div>
    );
  }
}

Root.propTypes = {
  history: PropTypes.object.isRequired
};
