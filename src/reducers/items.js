import { ADD_ITEM, DELETE_ITEM, EDIT_ITEM, FETCH_ITEMS } from '../constants/ActionTypes';

const initialState = [];

export default function items(state = initialState, action) {
  switch (action.type) {

    case ADD_ITEM:
      return [...state, action.item];

    case DELETE_ITEM:
      return state.filter(todo =>
        todo.id !== action.id
      );

    case EDIT_ITEM:
      return state.map(item =>
        item.id === action.id ?
          Object.assign({}, item, { id: action.id, description: action.description, ttn: action.ttn }) :
          item
      );

    case FETCH_ITEMS:
      return [...state, action.items];

    default:
      return state;
  }
}
